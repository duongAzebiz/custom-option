<?php
class Magebuzz_Advanceoptions_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {	
		$this->loadLayout();  
		$this->_initLayoutMessages('advanceoptions/session');
		$this->renderLayout();
    }
}