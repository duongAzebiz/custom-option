<?php
  class Magebuzz_Advanceoptions_Helper_Catalog_Product_Configuration extends  Mage_Catalog_Helper_Product_Configuration{

    public function getCustomOptions(Mage_Catalog_Model_Product_Configuration_Item_Interface $item)
    {
      $product = $item->getProduct();
      $options = array();
      $optionIds = $item->getOptionByCode('option_ids');
      if ($optionIds) {
        $options = array();
        foreach (explode(',', $optionIds->getValue()) as $optionId) {
          $option = $product->getOptionById($optionId);
          if ($option) {
            $itemOption = $item->getOptionByCode('option_' . $option->getId());
            $buy_request = Mage::getModel('sales/quote_item_option')->getCollection()
              ->addFieldToFilter('item_id', $itemOption->getData('item_id'))
              ->addFieldToFilter('product_id', $itemOption->getData('product_id'))
              ->addFieldToFilter('code', 'info_buyRequest')
              ->getFirstItem()
              ->getValue();
            if($buy_request){
              $arrayBuyRequest = unserialize($buy_request);
              $optionOfProduct = Mage::getModel('catalog/product_option_value')
              ->getCollection()
              ->addFieldToFilter('option_id', $optionId)
              ->addFieldToFilter('option_type_id', $itemOption->getValue())->getFirstItem();
              $childSelected = array();
              if($childOption = $optionOfProduct->getChildOption()){
                $childOption = explode(',', $childOption);
                foreach($childOption as $key=> $value){
                      $childSelected[$value] = $arrayBuyRequest['option_id_'.$value];
                }
              }
            }
            $group = $option->groupFactory($option->getType())
              ->setOption($option)
              ->setConfigurationItem($item)
              ->setConfigurationItemOption($itemOption);

            if ('file' == $option->getType()) {
              $downloadParams = $item->getFileDownloadParams();
              if ($downloadParams) {
                $url = $downloadParams->getUrl();
                if ($url) {
                  $group->setCustomOptionDownloadUrl($url);
                }
                $urlParams = $downloadParams->getUrlParams();
                if ($urlParams) {
                  $group->setCustomOptionUrlParams($urlParams);
                }
              }
            }

            $options[] = array(
              'label' => $option->getTitle(),
              'value' => $group->getFormattedOptionValue($itemOption->getValue()),
              'print_value' => $group->getPrintableOptionValue($itemOption->getValue()),
              'option_id' => $option->getId(),
              'option_type' => $option->getType(),
              'custom_view' => $group->isCustomizedView(),
              'child_option' => $childSelected
            );
          }
        }
      }

      $addOptions = $item->getOptionByCode('additional_options');
      if ($addOptions) {
        $options = array_merge($options, unserialize($addOptions->getValue()));
      }

      return $options;
    }
  }