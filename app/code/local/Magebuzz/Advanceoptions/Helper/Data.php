<?php

class Magebuzz_Advanceoptions_Helper_Data extends Mage_Core_Helper_Abstract
{
  public function getCustomChildOption($optionId, $optionValue, $productOptions){
    $modelOptionvalue = Mage::getModel('catalog/product_option_value')
      ->getCollection()
      ->addFieldToFilter('option_id', $optionId)
      ->addFieldToFilter('option_type_id', $optionValue)
      ->getFirstItem()->getChildOption();
    $buyRequestOption = $productOptions['info_buyRequest'];
    $valueOption = array();
    if($modelOptionvalue){
      foreach(explode(',',$modelOptionvalue) as $key => $value){
        $modelAdvanceOption = Mage::getModel('advanceoptions/advanceoptions')->load($value);
        $valueOption[$modelAdvanceOption->getTitle()] = $buyRequestOption['option_id_'.$value];
      }
    }
    return $valueOption;
  }

}