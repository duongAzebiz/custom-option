<?php

class Magebuzz_Advanceoptions_Model_Mysql4_Advanceoptions_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('advanceoptions/advanceoptions');
    }
}