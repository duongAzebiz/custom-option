<?php

class Magebuzz_Advanceoptions_Model_Mysql4_Advanceoptions extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the advanceoptions_id refers to the key field in your database table.
        $this->_init('advanceoptions/advanceoptions', 'advanceoptions_id');
    }
}