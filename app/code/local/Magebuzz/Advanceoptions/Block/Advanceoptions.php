<?php
class Magebuzz_Advanceoptions_Block_Advanceoptions extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getAdvanceoptions()     
     { 
        if (!$this->hasData('advanceoptions')) {
            $this->setData('advanceoptions', Mage::registry('advanceoptions'));
        }
        return $this->getData('advanceoptions');
        
    }
}