<?php
class Magebuzz_Advanceoptions_Block_Adminhtml_Advanceoptions extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_advanceoptions';
    $this->_blockGroup = 'advanceoptions';
    $this->_headerText = Mage::helper('advanceoptions')->__('Options Manager');
    $this->_addButtonLabel = Mage::helper('advanceoptions')->__('Add Option');
    parent::__construct();
  }
}