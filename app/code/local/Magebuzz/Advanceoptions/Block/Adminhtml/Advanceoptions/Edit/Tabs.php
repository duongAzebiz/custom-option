<?php

class Magebuzz_Advanceoptions_Block_Adminhtml_Advanceoptions_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('advanceoptions_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('advanceoptions')->__('Item Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('advanceoptions')->__('Item Information'),
          'title'     => Mage::helper('advanceoptions')->__('Item Information'),
          'content'   => $this->getLayout()->createBlock('advanceoptions/adminhtml_advanceoptions_edit_tab_main')->toHtml(),
      ));  
      return parent::_beforeToHtml();
  }
}