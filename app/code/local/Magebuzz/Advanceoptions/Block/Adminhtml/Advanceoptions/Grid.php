<?php

class Magebuzz_Advanceoptions_Block_Adminhtml_Advanceoptions_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{
	  parent::__construct();
	  $this->setId('advanceoptionsGrid');
	  $this->setUseAjax(true);
	  $this->setDefaultSort('advanceoptions_id');
	  $this->setDefaultDir('ASC');
	  $this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection()
	{
	  $collection = Mage::getModel('advanceoptions/advanceoptions')->getCollection();
	  $this->setCollection($collection);
	  return parent::_prepareCollection();
	}

	protected function _prepareColumns()
	{
	  $this->addColumn('advanceoptions_id', array(
		  'header'    => Mage::helper('advanceoptions')->__('ID'),
		  'align'     =>'right',
		  'width'     => '50px',
		  'index'     => 'advanceoptions_id',
	  ));

	  $this->addColumn('title', array(
		  'header'    => Mage::helper('advanceoptions')->__('Title'),
		  'align'     =>'left',
		  'index'     => 'title',
	  ));

    $this->addColumn('type', array(
      'header'    => Mage::helper('advanceoptions')->__('Type'),
      'align'     =>'left',
      'index'     => 'type',
      'type'      => 'options',
      'options'  => array(
        '1' => 'Text Field',
        '2' => 'Fonts Type',
        '3' => 'Logo Size'
      )
    ));

	  $this->addColumn('status', array(
		  'header'    => Mage::helper('advanceoptions')->__('Status'),
		  'align'     => 'left',
		  'width'     => '80px',
		  'index'     => 'status',
		  'type'      => 'options',
		  'options'   => array(
			  1 => 'Enabled',
			  2 => 'Disabled',
		  ),
	  ));
	  
		$this->addColumn('action',
			array(
				'header'    =>  Mage::helper('advanceoptions')->__('Action'),
				'width'     => '100',
				'type'      => 'action',
				'getter'    => 'getId',
				'actions'   => array(
					array(
						'caption'   => Mage::helper('advanceoptions')->__('Edit'),
						'url'       => array('base'=> '*/*/edit'),
						'field'     => 'id'
					)
				),
				'filter'    => false,
				'sortable'  => false,
				'index'     => 'stores',
				'is_system' => true,
		));
		
		$this->addExportType('*/*/exportCsv', Mage::helper('advanceoptions')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('advanceoptions')->__('XML'));
	  
	  return parent::_prepareColumns();
	}

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('advanceoptions_id');
        $this->getMassactionBlock()->setFormFieldName('advanceoptions');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('advanceoptions')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('advanceoptions')->__('Are you sure?')
        ));

        $statuses = Mage::getSingleton('advanceoptions/status')->getOptionArray();

        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('advanceoptions')->__('Change status'),
             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('advanceoptions')->__('Status'),
                         'values' => $statuses
                     )
             )
        ));
        return $this;
    }

	public function getRowUrl($row)
	{
	  return $this->getUrl('*/*/edit', array('id' => $row->getId()));
	}
	public function getGridUrl()
	{
		return $this->getUrl('*/*/grid', array('_current'=> true));
	}  

}