<?php

class Magebuzz_Advanceoptions_Block_Adminhtml_Advanceoptions_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'advanceoptions';
        $this->_controller = 'adminhtml_advanceoptions';
        
        $this->_updateButton('save', 'label', Mage::helper('advanceoptions')->__('Save Item'));
        $this->_updateButton('delete', 'label', Mage::helper('advanceoptions')->__('Delete Item'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('advanceoptions_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'advanceoptions_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'advanceoptions_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('advanceoptions_data') && Mage::registry('advanceoptions_data')->getId() ) {
            return Mage::helper('advanceoptions')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('advanceoptions_data')->getTitle()));
        } else {
            return Mage::helper('advanceoptions')->__('Add Item');
        }
    }
}