<?php

class Magebuzz_Advanceoptions_Block_Adminhtml_Advanceoptions_Edit_Tab_Main extends Mage_Adminhtml_Block_Widget_Form
{

  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
	  $form->setHtmlIdPrefix('advanceoptions_');
      $this->setForm($form);
      $fieldset = $form->addFieldset('advanceoptions_form', array('legend'=>Mage::helper('advanceoptions')->__('Item information')));
     
      $fieldset->addField('title', 'text', array(
          'label'     => Mage::helper('advanceoptions')->__('Title'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'title',
      ));

    $fieldset->addField('description', 'textarea', array(
      'label'     => Mage::helper('advanceoptions')->__('Description'),
      'class'     => 'required-entry',
      'required'  => true,
      'name'      => 'description',
    ));

    $fieldset->addField('status', 'select', array(
      'label'     => Mage::helper('advanceoptions')->__('Status'),
      'name'      => 'status',
      'values'    => array(
        array(
          'value'     => 1,
          'label'     => Mage::helper('advanceoptions')->__('Enabled'),
        ),

        array(
          'value'     => 2,
          'label'     => Mage::helper('advanceoptions')->__('Disabled'),
        ),
      ),
    ));

    $fieldset->addField('type', 'select', array(
      'label'     => Mage::helper('advanceoptions')->__('Type'),
      'class'     => 'required-entry',
      'required'  => true,
      'name'      => 'type',
      'onclick' => "",
      'onchange' => "",
      'value'  => '1',
      'values' => array(''=>'Please Select..','1' => 'Text Field','2' => 'Fonts Type', '3' => 'Logo Size'),
      'disabled' => false,
      'readonly' => false,
      'tabindex' => 1
    ));



      if ( Mage::getSingleton('adminhtml/session')->getAdvanceoptionsData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getAdvanceoptionsData());
          Mage::getSingleton('adminhtml/session')->setAdvanceoptionsData(null);
      } elseif ( Mage::registry('advanceoptions_data') ) {
          $form->setValues(Mage::registry('advanceoptions_data')->getData());
      }
      return parent::_prepareForm();
  }
}