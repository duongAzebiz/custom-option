<?php

$installer = $this;

$installer->startSetup();

$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('advanceoptions')};
CREATE TABLE {$this->getTable('advanceoptions')} (
  `advanceoptions_id` int(11) unsigned NOT NULL auto_increment,
  `title` varchar(255) NOT NULL default '',
  `description` varchar(255) NOT NULL default '',
  `status` smallint(6) NOT NULL default '0',
  `type` varchar(50) NOT NULL default '1',
  PRIMARY KEY (`advanceoptions_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    ");

$installer->endSetup();