<?php

/* @var $installer Mage_Core_Model_Resource_Setup */

$installer = $this;

$installer->getConnection()

  ->addColumn($installer->getTable('catalog/product_option'), 'is_custom', 'int(11) default 0 ');

$installer->endSetup();