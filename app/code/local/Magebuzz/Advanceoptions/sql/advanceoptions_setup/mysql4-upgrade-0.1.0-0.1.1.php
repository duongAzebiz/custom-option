<?php

/* @var $installer Mage_Core_Model_Resource_Setup */

$installer = $this;

$installer->getConnection()

  ->addColumn($installer->getTable('catalog/product_option_type_value'), 'child_option', 'VARCHAR(128) NULL');

$installer->endSetup();